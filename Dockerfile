FROM python:3.8.6-alpine3.12

RUN apk add --no-cache postgresql-dev \
    && apk add --no-cache --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    linux-headers \
    && pip3 install --no-cache-dir uwsgi flask redis psycopg2-binary  \
    && apk del --no-cache .build-deps

RUN adduser --home /srv --disabled-password www-data

EXPOSE 5000

WORKDIR /srv
ADD server.py uwsgi.ini ./
ADD templates ./templates
ENTRYPOINT ["uwsgi"]
CMD ["--ini", "uwsgi.ini"]