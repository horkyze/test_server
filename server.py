import os
import redis
import psycopg2
from flask import Flask, render_template
from datetime import datetime

PG_DB = os.environ['PG_DB']
PG_USER = os.environ['PG_USER']
PG_PASS = os.environ['PG_PASS']
PG_HOST = os.environ['PG_HOST']
REDIS_HOST = os.environ['REDIS_HOST']

app = Flask(__name__)
redis = redis.Redis(REDIS_HOST)
psql = psycopg2.connect(database=PG_DB, user=PG_USER, password=PG_PASS, host=PG_HOST)


def increment_counter():
    counter = redis.incr('view_counter')
    return counter


def get_counter():
    return redis.get('view_counter')


def save_timestamp(timestamp, view_count):
    cursor = psql.cursor()
    data = (view_count, timestamp)
    cursor.execute('INSERT INTO counter (view_id, timestamp) VALUES (%s, %s)', data)
    psql.commit()


@app.route('/')
def serve():
    view_count = increment_counter()
    now = datetime.now()
    save_timestamp(now, view_count)

    return render_template('index.html', view_count=view_count, timestamp=now)
